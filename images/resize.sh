#!/bin/bash
# Resizes images
# Requires imagemagick
# Based off https://github.com/mavieth/image-resizer

SIZE="300kb"
SCALE="100%"

for image in `ls *.{png,jpeg,jpg,JPG} -R 2>/dev/null`; do

	# Save original copy in full directory
	cp $image full/$image

	# Convert
	convert $image -define jpeg:extent=$SIZE -scale $SCALE -auto-orient $image

	# Move converted image to thumbnail directory
	mv $image thumb/

	# Very bad code
	cur=`pwd`
	result="${cur%"${cur##*[!/]}"}"
	result=${result##*/}
	dir=`pwd`
	parentdir=$(dirname "$dir")
	parentbase=$(basename "$parentdir")
	grandpadir=$(basename "$(dirname $parentdir)")
	grandpabase=$(basename "$grandpadir")

	echo "{{< imgy \"FILLTHISIN\" \"/$grandpabase/$parentbase/$result/full/$image\"  \"/$grandpabase/$parentbase/$result/thumb/$image\"  >}}"
done
